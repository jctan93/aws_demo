#!/usr/bin/env bash

sudo apt-get update
sudo apt-get install -y \
	apt-transport-https \
	ca-certificates \
	curl \
	gnupg \
	lsb-release
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --batch --yes --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo \
	"deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
	$(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get install -y docker-ce docker-ce-cli containerd.io
sudo mkdir -p ~/custom_html
sudo echo '<h2>HELLO THERE!</h2><a href="https://knowyourmeme.com/memes/hello-there"><img src="https://i.kym-cdn.com/entries/icons/original/000/029/079/hellothere.jpg"></a>' > ~/custom_html/index.html
sudo docker run -it --rm -d -p 80:80 --name test_nginx -v ~/custom_html:/usr/share/nginx/html nginx:1.18.0
