variable "aws_region" {
    default = "us-east-1"
}

provider "aws" {
    region = var.aws_region
}

# aws_key_pair.demo_pub_key
resource "aws_key_pair" "demo_pub_key"{
    key_name    = "demo_pub_key"
    public_key  = file("public.pub")
}

# aws_eip.demo_eip:
resource "aws_eip" "demo_eip" {
    vpc                  = true
}

# aws_vpc.demo_vpc:
resource "aws_vpc" "demo_vpc" {
    assign_generated_ipv6_cidr_block = false
    cidr_block                       = "10.0.0.0/16"
    enable_classiclink               = false
    enable_classiclink_dns_support   = false
    enable_dns_hostnames             = true
    enable_dns_support               = true
    instance_tenancy                 = "default"
    tags                             = {
        "Name" = "demo"
    }
    tags_all                         = {
        "Name" = "demo"
    }
}

# aws_internet_gateway.demo_internet_gateway
resource "aws_internet_gateway" "demo_internet_gateway" {
    tags     = {}
    vpc_id   = aws_vpc.demo_vpc.id
}

# aws_nat_gateway.demo_nat_gateway:
resource "aws_nat_gateway" "demo_nat_gateway" {
    allocation_id        = aws_eip.demo_eip.id
    subnet_id            = aws_subnet.public_subnet.id
    tags                 = {}
}

# aws_subnet.private_subnet:
resource "aws_subnet" "private_subnet" {
    assign_ipv6_address_on_creation = false
    availability_zone               = "us-east-1a"
    cidr_block                      = "10.0.1.0/24"
    map_public_ip_on_launch         = false
    tags                            = {
        "Name" = "Private subnet demo"
    }
    tags_all                        = {
        "Name" = "Private subnet demo"
    }
    vpc_id                          = aws_vpc.demo_vpc.id

    timeouts {}
}

# aws_subnet.public_subnet:
resource "aws_subnet" "public_subnet" {
    assign_ipv6_address_on_creation = false
    availability_zone               = "us-east-1a"
    cidr_block                      = "10.0.0.0/24"
    map_public_ip_on_launch         = false
    tags                            = {
        "Name" = "Public subnet demo"
    }
    tags_all                        = {
        "Name" = "Public subnet demo"
    }
    vpc_id                          = aws_vpc.demo_vpc.id

    timeouts {}
}

# aws_route_table.demo_private_rt:
resource "aws_route_table" "demo_private_rt" {
    propagating_vgws = []
    tags             = {}
    vpc_id           = aws_vpc.demo_vpc.id
}

# aws_route_table.demo_public_rt:
resource "aws_route_table" "demo_public_rt" {
    propagating_vgws = []
    tags             = {}
    vpc_id           = aws_vpc.demo_vpc.id
}

# aws_route.private_route_2:
resource "aws_route" "private_route_2" {
    destination_cidr_block = "0.0.0.0/0"
    nat_gateway_id         = aws_nat_gateway.demo_nat_gateway.id
    route_table_id         = aws_route_table.demo_private_rt.id

    timeouts {}
}

# aws_route.public_route_2:
resource "aws_route" "public_route_2" {
    destination_cidr_block = "0.0.0.0/0"
    gateway_id             = aws_internet_gateway.demo_internet_gateway.id
    route_table_id         = aws_route_table.demo_public_rt.id

    timeouts {}
}

# aws_route_table_association.public_ra:
resource "aws_route_table_association" "public_ra" {
    route_table_id = aws_route_table.demo_public_rt.id
    subnet_id      = aws_subnet.public_subnet.id
}

# aws_route_table_association.private_ra:
resource "aws_route_table_association" "private_ra" {
    route_table_id = aws_route_table.demo_private_rt.id
    subnet_id      = aws_subnet.private_subnet.id
}

# aws_security_group.demo_internal_security_group.id:
resource "aws_security_group" "demo_internal_security_group" {
    egress      = [
        {
            cidr_blocks      = [
                "0.0.0.0/0",
            ]
            description      = ""
            from_port        = 0
            ipv6_cidr_blocks = []
            prefix_list_ids  = []
            protocol         = "-1"
            security_groups  = []
            self             = false
            to_port          = 0
        },
    ]
    ingress     = [
        {
            cidr_blocks      = [
                "0.0.0.0/0",
            ]
            description      = ""
            from_port        = 22
            ipv6_cidr_blocks = []
            prefix_list_ids  = []
            protocol         = "tcp"
            security_groups  = []
            self             = false
            to_port          = 22
        },
        {
            cidr_blocks      = [
                "118.189.0.0/16",
                "116.206.0.0/16",
                "223.25.0.0/16",
                "175.140.117.21/32",
            ]
            description      = ""
            from_port        = 80
            ipv6_cidr_blocks = []
            prefix_list_ids  = []
            protocol         = "tcp"
            security_groups  = []
            self             = false
            to_port          = 80
        },
    ]
    name        = "demo-internal"
    tags        = {}
    vpc_id      = aws_vpc.demo_vpc.id

    timeouts {}
}

# aws_instance.demo_instance:
resource "aws_instance" "demo_instance" {
    ami                          = "ami-042e8287309f5df03"
    user_data = file("resources/test.sh")
    associate_public_ip_address  = false
    availability_zone            = "us-east-1a"
    disable_api_termination      = false
    ebs_optimized                = false
    get_password_data            = false
    hibernation                  = false
    instance_type                = "t2.micro"
    ipv6_address_count           = 0
    ipv6_addresses               = []
    key_name                     = aws_key_pair.demo_pub_key.key_name
    monitoring                   = false
    secondary_private_ips        = []
    security_groups              = []
    source_dest_check            = true
    subnet_id                    = aws_subnet.private_subnet.id
    tags                         = {
        "demo" = "demo"
    }
    tenancy                      = "default"
    vpc_security_group_ids       = [
        aws_security_group.demo_internal_security_group.id,
    ]

    credit_specification {
        cpu_credits = "standard"
    }

    enclave_options {
        enabled = false
    }

    metadata_options {
        http_endpoint               = "enabled"
        http_put_response_hop_limit = 1
        http_tokens                 = "optional"
    }

    timeouts {}
}

# aws_lb_target_group.demo_lb_target_22:
resource "aws_lb_target_group" "demo_lb_target_22" {
    deregistration_delay = 300
    name                 = "tf-demo-target-22"
    port                 = 22
    protocol             = "TCP"
    proxy_protocol_v2    = false
    tags                 = {}
    target_type          = "instance"
    vpc_id               = aws_vpc.demo_vpc.id
}

# aws_lb_target_group.demo_lb_target_80:
resource "aws_lb_target_group" "demo_lb_target_80" {
    deregistration_delay = 300
    name                 = "tf-demo-target-80"
    port                 = 80
    protocol             = "TCP"
    proxy_protocol_v2    = false
    tags                 = {}
    target_type          = "instance"
    vpc_id               = aws_vpc.demo_vpc.id
}

# aws_lb.demo_load_balancer:
resource "aws_lb" "demo_load_balancer" {
    enable_cross_zone_load_balancing = false
    enable_deletion_protection       = false
    internal                         = false
    ip_address_type                  = "ipv4"
    load_balancer_type               = "network"
    name                             = "demo-load-balancer"
    security_groups                  = []
    subnets                          = [
        aws_subnet.public_subnet.id,
    ]
    tags                             = {}
    timeouts {}
}

resource "aws_lb_target_group_attachment" "demo_register_80" {
  target_group_arn = aws_lb_target_group.demo_lb_target_80.arn
  target_id        = aws_instance.demo_instance.id
  port             = 80
}

resource "aws_lb_target_group_attachment" "demo_register_22" {
  target_group_arn = aws_lb_target_group.demo_lb_target_22.arn
  target_id        = aws_instance.demo_instance.id
  port             = 22
}

# aws_lb_listener.test_listener_22:
resource "aws_lb_listener" "test_listener_22" {
    load_balancer_arn = aws_lb.demo_load_balancer.arn
    port              = 22
    protocol          = "TCP"

    default_action {
        target_group_arn = aws_lb_target_group.demo_lb_target_22.arn
        type             = "forward"
    }

    timeouts {}
}

# aws_lb_listener.test_listener:
resource "aws_lb_listener" "test_listener_80" {
    load_balancer_arn = aws_lb.demo_load_balancer.arn
    port              = 80
    protocol          = "TCP"

    default_action {
        target_group_arn = aws_lb_target_group.demo_lb_target_80.arn
        type             = "forward"
    }

    timeouts {}
}

resource "aws_iam_user" "demo-user" {
  name = "demo-iam-user"
}

resource "aws_iam_user_policy_attachment" "demo_attach_ro" {
  user       = aws_iam_user.demo-user.name
  policy_arn = "arn:aws:iam::aws:policy/ReadOnlyAccess"
}

#resource "aws_iam_user_login_profile" "u" {
#  user    = aws_iam_user.demo-user.name
#  pgp_key = "keybase:jctan93"
#}

#output "password" {
#  value = aws_iam_user_login_profile.u.encrypted_password
#}
